console.log("Hello World")
// Objective 1 Details

let firstName = "Sharlene";
let lastName = "Tapia";
let age = 28;
let hobbies = [
    "building and painting warhammer minis",
    "playing JRPG",
    "consuming media"
];
let workAddress = {
    houseNumber: "Unit 4221, South Tower, Zinnia Towers",
    street: "1211 EDSA",
    city: "Quezon City",
    state: "Philippines"
};

// Objective 2 Details

let name = "Steve Rogers";
let currentAge = 40;
let friends = [
    "Tony",
    "Bruce",
    "Thor",
    "Natasha",
    "Clint",
    "Nick"
];
let fullProfile = {
    userName: "captain_america",
    fullName: "Steve Rogers",
    age: currentAge,
    isActive: false
};
let bestFriend = "Bucky Barnes";
let locationFound = "Arctic Ocean";

// Objective 1 Console log
console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies: ")
console.log(hobbies);
console.log("Work Address: ");
console.log(workAddress);

// Objective 2 Console log
console.log("My full name is: " + name);
console.log("My current age is: " + currentAge);
console.log("My friends are: ")
console.log(friends);
console.log("My Full Profile: ");
console.log(fullProfile);
console.log("My best friend is: " + bestFriend);
console.log("I was found frozen in: " + locationFound);
